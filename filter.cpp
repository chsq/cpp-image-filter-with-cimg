#include <string>
#include <iostream>
#include <vector>
#include "CImg.h"
#include <fstream>
#include <sstream>

using namespace std;
/*
this program applies a basic blur to images
by calculating the avg of each color channel in the kernel
then applies that value to the current pixel creating a blur effect
*/
vector<vector<int>> read_kernel(string fn){
    vector<vector<int>> kernel_data;
    string line;
    ifstream infile(fn);

    while(std::getline(infile, line)){
      istringstream iss(line);
      int c;
      vector<int> kernel_row;
      while (iss >> c){
        kernel_row.push_back(c);
      }
      kernel_data.push_back(kernel_row);
    }
    return kernel_data;
}

int main(int argc, char* argv[]){
  string iname;
  string oname;
  cimg_library::CImg<float> img_in;
  cimg_library::CImg<float> img_out;
  iname = argv[1];
  oname = argv[2];

  img_in.load(argv[1]);
  img_out.load(argv[1]);

  vector<vector<int>> current_kernel = read_kernel(argv[3]);
  int kernel_radius = (current_kernel.size()-1)/2;
  int sum_r,sum_g,sum_b;
  int divis=0;
  //loop over every pixel in the image
  for (int x = 0; x < img_in.width(); x++){
    for (int y = 0; y < img_in.height(); y++){
      sum_r = 0;
      sum_g = 0;
      sum_b = 0;
      divis = 0;
      //loop over the kernel
      int k = 0;
      for(int i = kernel_radius*-1; i <= kernel_radius;i++){
        int l = 0;
        for(int j = kernel_radius*-1; j <= kernel_radius; j++){
          //make sure the pixel is not out of bounds
          if(x + i > 0 && y + j > 0 && x + i < img_in.width() && y + j < img_in.height()){
            divis += current_kernel[k][l];
            sum_r += current_kernel[k][l]*(int)img_in(x + i,y + j,0,0);
            sum_g += current_kernel[k][l]*(int)img_in(x + i,y + j,0,1);
            sum_b += current_kernel[k][l]*(int)img_in(x + i,y + j,0,2);
          }
          l++;
        }
        k++;
      }
      //find the avg of all the color channels and apply it to out img
      if(divis != 0){
        sum_r /= divis;
        sum_g /= divis;
        sum_b /= divis;
      }

      img_out(x,y,0,0) = sum_r;
      img_out(x,y,0,1) = sum_g;
      img_out(x,y,0,2) = sum_b;
    }
  }
  img_out.save(oname.c_str());

  return 0;
}
